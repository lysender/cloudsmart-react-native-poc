[![CircleCI](https://circleci.com/bb/lysender/cloudsmart-react-native-poc.svg?style=svg)](https://circleci.com/bb/lysender/cloudsmart-react-native-poc)

# React Native Mobile App POC

## Links

* [React](https://reactjs.org/)
* [React Native](https://facebook.github.io/react-native/)
* [Expo](https://expo.io/)
 
